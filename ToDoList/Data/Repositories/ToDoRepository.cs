﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ToDoListProject.Data.Entities;

namespace ToDoListProject.Data.Repositories
{
    public class ToDoRepository: IToDoRepository
    {
        private readonly ToDoListDbContext _context;

        public ToDoRepository(ToDoListDbContext context)
        {
            _context = context;
        }

        public ToDo Add(int toDoListId, ToDo toDo)
        { 
            toDo.ToDoListId = toDoListId;
            _context.Add(toDo);
            SaveChanges();

            return toDo;
        }

        public IEnumerable<ToDo> GetToDosByToDoListId(int toDoListId)
        {
            return _context.ToDos.Where(t => t.ToDoList.Id == toDoListId);
        }

        public ToDo GetToDo(int toDoListId, int id)
        {
            return  _context.ToDos.FirstOrDefault(t => t.Id == id && t.ToDoList.Id == toDoListId);
        }

        public ToDo UpdateTodo(int toDoListId, int oldToDoId, ToDo toDoFromPayload)
        {
            var entity = _context.ToDos.Update(toDoFromPayload);
            entity.State = EntityState.Modified;
            SaveChanges();

            return toDoFromPayload;
        }

        public void RemoveToDo(int toDoListId, int id)
        {
            var toDo = GetToDo(toDoListId, id);
            if (toDo != null)
            {
                _context.ToDos.Remove(toDo);
                SaveChanges();

            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
