﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ToDoListProject.Data.Repositories
{
    public class ToDoListRepository : IToDoListRepository
    {

        private readonly ToDoListDbContext _context;

        public ToDoListRepository(ToDoListDbContext context)
        {
            _context = context;
        }

        public Entities.ToDoList AddToDoList(Entities.ToDoList toDoList)
        {
            _context.Add(toDoList);
            SaveChanges();

            return toDoList;
        }

        public IEnumerable<Entities.ToDoList> GetToDoLists()
        {
            return _context.ToDoLists;
        }

        public Entities.ToDoList GetToDoList(int id)
        {
            return _context.ToDoLists.Find(id);
        }

        public void RemoveToDoList(int id)
        {
            var todoList = GetToDoList(id);
            if (todoList != null)
            {
                _context.ToDoLists.Remove(todoList);
                SaveChanges();
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
