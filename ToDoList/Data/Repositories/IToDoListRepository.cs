﻿using System.Collections.Generic;

namespace ToDoListProject.Data.Repositories
{
    public interface IToDoListRepository
    {
        Entities.ToDoList AddToDoList(Entities.ToDoList tList);
        IEnumerable<Entities.ToDoList> GetToDoLists();
        Entities.ToDoList GetToDoList(int id);
        void RemoveToDoList(int id);
        void SaveChanges();
    }
}