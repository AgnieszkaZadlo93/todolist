﻿using System.Collections.Generic;
using ToDoListProject.Data.Entities;

namespace ToDoListProject.Data.Repositories
{
    public interface IToDoRepository
    {
        ToDo Add(int toDoListId, ToDo toDo);
        IEnumerable<ToDo> GetToDosByToDoListId(int toDoListId);
        ToDo GetToDo(int toDoListId, int id);
        void RemoveToDo(int toDoListId, int id);
        void SaveChanges();
    }
}