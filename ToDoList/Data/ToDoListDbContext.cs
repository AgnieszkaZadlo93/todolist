﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ToDoListProject.Data.Entities;


namespace ToDoListProject.Data
{
    public class ToDoListDbContext : DbContext
        {
            public ToDoListDbContext(DbContextOptions<ToDoListDbContext> options)
                : base(options)
            {

            }
            public DbSet<ToDo> ToDos { get; set; }
            public DbSet<Entities.ToDoList> ToDoLists { get; set; }

    }
    

}
