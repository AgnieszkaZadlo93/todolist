﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ToDoListProject.Data.Entities;
using ToDoListProject.Models;

namespace ToDoListProject.Data
{
    public class ToDoListProfile : Profile
    {
        public ToDoListProfile()
        {
            this.CreateMap<ToDo, ToDoModel>()
                .ReverseMap();

            this.CreateMap<Entities.ToDoList, ToDoListModel>()
                .ReverseMap();

        }
    }
}
