﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoListProject.Models

{
    public class ToDoListModel
    {
        [Required]
        public string Title { get; set; }
    }
}
