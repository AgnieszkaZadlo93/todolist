﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoListProject.Data.Repositories;
using ToDoListProject.Models;

namespace ToDoListProject.Controllers
{
    [Route("api/todo-lists")]
    [ApiController]
    public class ToDoListsController : ControllerBase
    {
        private readonly IToDoListRepository _repository;
        private readonly IMapper _mapper;

        public ToDoListsController(IToDoListRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<Models.ToDoListModel> Get()
        {
            var results = _repository.GetToDoLists();
            return _mapper.Map<ToDoListModel[]>(results);
        }

        [HttpGet("{id}")]
        public ActionResult<Models.ToDoListModel> Get(int id)
        {
            var toDoList = _repository.GetToDoList(id);
            if (toDoList == null) return NotFound();

            return _mapper.Map<ToDoListModel>(toDoList);
        }

        [HttpPost]
        public ActionResult<Models.ToDoListModel> Post([FromBody] Models.ToDoListModel toDoListModel)
        {
            var toDoList = _mapper.Map<Data.Entities.ToDoList>(toDoListModel);
            var createdToDoList = _repository.AddToDoList(toDoList);

            return _mapper.Map<ToDoListModel>(createdToDoList);
        }

        [HttpPut("{id}")]
        public ActionResult<ToDoListModel> Put(int id, [FromBody] Models.ToDoListModel toDoListModel)
        {
            var oldDoDoList = _repository.GetToDoList(id);
            if (oldDoDoList == null) return NotFound("Could not find given to do");

            _mapper.Map(toDoListModel, oldDoDoList);
            _repository.SaveChanges();

            return toDoListModel;
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var todo = _repository.GetToDoList(id);
            if (todo == null) return NotFound("Could not find given to do");

            _repository.RemoveToDoList(id);

            return NoContent();
        }
    }
}