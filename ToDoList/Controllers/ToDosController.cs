﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ToDoListProject.Data.Entities;
using ToDoListProject.Data.Repositories;
using ToDoListProject.Models;

namespace ToDoList.Controllers
{
    [Route("api/todo-lists/{toDoListId}/todos")]
    public class ToDosController : Controller
    {

        private readonly IToDoRepository _repository;
        private readonly IMapper _mapper;

        public ToDosController(IToDoRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<ToDoModel> GetByToDoListId(int toDoListId)
        {
            var results=  _repository.GetToDosByToDoListId(toDoListId);
            return _mapper.Map<ToDoModel[]>(results);
        }

        [HttpGet("{id}")]
        public ActionResult<ToDoModel> Get(int toDoListId, int id)
        {
            var toDo = _repository.GetToDo(toDoListId, id);
            if (toDo == null) return NotFound();

            return _mapper.Map<ToDoModel>(toDo);
        }

        [HttpPost]
        public ActionResult<ToDoModel> Post(int toDoListId, [FromBody]ToDoModel toDoModel)
        {
            var toDo = _mapper.Map<ToDo>(toDoModel);

            ToDo createdToDo = _repository.Add(toDoListId, toDo);

            return _mapper.Map<ToDoModel>(createdToDo);
        }

        [HttpPut("{id}")]
        public ActionResult<ToDoModel> Put(int toDoListId, int id, [FromBody]ToDoModel toDoModel)
        {
            var oldToDo = _repository.GetToDo(toDoListId , id);
            if (oldToDo == null) return NotFound("Could not find given to do");

            _mapper.Map(toDoModel, oldToDo);
            _repository.SaveChanges();

            return toDoModel;
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int toDoListId, int id)
        {
            var todo = _repository.GetToDo(toDoListId, id);
            if (todo == null) return NotFound("Could not find given to do");

            _repository.RemoveToDo(toDoListId, id);

            return NoContent();
        }
    }
}
